﻿using NimbusFox.FoxCore.Dependencies.Newtonsoft.Json;

namespace NimbusFox.StaxelStates.Classes.Config {
    internal class Config {
        [JsonProperty("includeY")]
        internal bool IncludeY { get; }
        [JsonProperty("maxTiles")]
        internal long MaxTiles { get; }
        [JsonProperty("minTiles")]
        internal long MinTiles { get; }
        [JsonProperty("costPerTile")]
        internal long CostPerTile { get; }
        [JsonProperty("showBorderForSeconds")]
        internal long ShowBorderForSeconds { get; }
        [JsonProperty("tilesBetweenClaims")]
        internal long TilesBetweenClaims { get; }
        [JsonProperty("firstConeMaxDistanceFromSign")]
        internal long FirstConeMaxDistanceFromSign { get; }
        [JsonProperty("unclaimedLandName")]
        internal string UnclaimedLandName { get; }

        internal Config() {
            IncludeY = false;
            MaxTiles = 100;
            MinTiles = 8;
            CostPerTile = 20;
            ShowBorderForSeconds = 10;
            TilesBetweenClaims = 3;
            FirstConeMaxDistanceFromSign = 4;
            UnclaimedLandName = "FarmLand";
        }

        internal void Save() {
            EstatesHook.FoxCore.ConfigDirectory.WriteFile("config.json", this, true);
        }
    }
}
