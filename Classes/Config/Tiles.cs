﻿using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Dependencies.Newtonsoft.Json;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates.Classes.Config {
    public class Tiles {
        [JsonProperty("coneBottom")]
        internal string ConeBottom { get; set; }
        [JsonProperty("coneTop")]
        internal string ConeTop { get; set; }
        [JsonProperty("pipe")]
        internal string Pipe { get; set; }
        [JsonProperty("tape")]
        internal string Tape { get; set; }

        internal Tiles() {
            ConeBottom = "nimbusfox.staxelstates.tile.coneBottom";
            ConeTop = "nimbusfox.staxelstates.tile.conTop";
            Pipe = "nimbusfox.staxelstates.tile.pipe";
            Tape = "nimbusfox.staxelstates.tile.tape";
        }
    }
}
