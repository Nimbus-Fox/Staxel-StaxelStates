﻿using Staxel.Items;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Dependencies.Newtonsoft.Json;

namespace NimbusFox.StaxelStates.Classes.Config {
    public class Items {
        [JsonProperty("builderLicense")]
        public string BuilderLicense { get; set; }
        [JsonProperty("guestLicense")]
        public string GuestLicense { get; set; }
        [JsonProperty("claimTool")]
        public string ClaimTool { get; set; }
        [JsonProperty("tickMark")]
        internal string TickMark { get; set; }
        [JsonProperty("xMark")]
        internal string XMark { get; set; }

        internal Items() {
            BuilderLicense = "nimbusfox.staxelstates.item.builderLicense";
            GuestLicense = "nimbusfox.staxelstates.item.guestLicense";
            ClaimTool = "nimbusfox.staxelstates.item.coneBottom";
            TickMark = "nimbusfox.staxelstates.item.tickmark";
            XMark = "nimbusfox.staxelstates.item.xmark";
        }
    }
}
