﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Classes {
    internal static class Helpers {
        public static uint GetConeRotation(VectorCubeI cube, Vector3I location) {
            if (EstatesHook.Config.IncludeY) {
                return 0;
            }
            if (location.X == cube.Start.X && location.Z == cube.Start.Z) {
                return 0;
            }

            if (location.X == cube.End.X && location.Z == cube.Start.Z) {
                return 3;
            }

            if (location.X == cube.Start.X && location.Z == cube.End.Z) {
                return 1;
            }

            if (location.X == cube.End.X && location.Z == cube.End.Z) {
                return 2;
            }

            return 0;
        }
    }
}
