﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Classes {
    public static class Extensions {
        public static void SetVector4F(this Blob blob, Vector4F vector) {
            blob.SetDouble("x", vector.X);
            blob.SetDouble("y", vector.Y);
            blob.SetDouble("z", vector.Z);
            blob.SetDouble("w", vector.W);
        }

        public static Vector4F GetVector4F(this Blob blob) {
            return new Vector4F((float)blob.GetDouble("x"), (float)blob.GetDouble("y"), (float)blob.GetDouble("z"), (float)blob.GetDouble("w"));
        }

        public static void SetColor(this Blob blob, Color color) {
            var c = color.ToVector4();

            var v4 = new Vector4F(c.X, c.Y, c.Z, c.W);

            SetVector4F(blob, v4);
        }

        public static Color GetColor(this Blob blob) {
            var v4 = blob.GetVector4F();

            var color = new Vector4(v4.X, v4.Y, v4.Z, v4.W);

            return new Color(color);
        }

        internal static T GetPrivatePropertyValue<T>(this object parentObject, string field) {
            return (T)parentObject.GetType().GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivatePropertyValue(this object parentObject, string field, object value) {
            parentObject.GetType().GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivateFieldValue<T>(this object parentObject, string field) {
            return (T)parentObject.GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivateFieldValue(this object parentObject, string field, object value) {
            parentObject.GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivatePropertyValue<T>(this object parentObject, string field, Type type) {
            return (T)type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivatePropertyValue(this object parentObject, string field, object value, Type type) {
            type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        internal static T GetPrivateFieldValue<T>(this object parentObject, string field, Type type) {
            return (T)type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        internal static void SetPrivateFieldValue(this object parentObject, string field, object value, Type type) {
            type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }
    }
}
