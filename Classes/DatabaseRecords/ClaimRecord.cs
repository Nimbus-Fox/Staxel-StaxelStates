﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using NimbusFox.FoxCore.Dependencies.Newtonsoft.Json;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Classes.DatabaseRecords {
    internal class ClaimRecord : BaseRecord {
        
        [JsonIgnore]
        public VectorSquareI Region2D { get; private set; }

        [JsonIgnore]
        public VectorCubeI Region3D { get; private set; }

        public Vector3I SignLocation {
            get =>_blob.Contains(nameof(SignLocation)) ? _blob.FetchBlob(nameof(SignLocation)).GetVector3I() : Vector3I.Zero;
            set {
                _blob.FetchBlob(nameof(SignLocation)).SetVector3I(value);
                Save();
            }
        }

        public string Owner {
            get => _blob.GetString(nameof(Owner), "");
            set {
                _blob.SetString(nameof(Owner), value);
                Save();
            }
        }

        public Vector3I Start {
            get => _blob.Contains(nameof(Start)) ? _blob.FetchBlob(nameof(Start)).GetVector3I() : Vector3I.Zero;
            set {
                _blob.FetchBlob(nameof(Start)).SetVector3I(value);
                Region2D = new VectorSquareI(Start, End);
                Region3D = new VectorCubeI(Start, End);
                Save();
            }
        }

        public Vector3I End {
            get => _blob.Contains(nameof(End)) ? _blob.FetchBlob(nameof(End)).GetVector3I() : Vector3I.Zero;
            set {
                _blob.FetchBlob(nameof(End)).SetVector3I(value);
                Region2D = new VectorSquareI(Start, End);
                Region3D = new VectorCubeI(Start, End);
                Save();
            }
        }

        public ClaimRecord(BlobDatabase database, Blob blob) : base(database, blob) { }
    }
}
