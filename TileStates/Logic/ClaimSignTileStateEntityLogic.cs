﻿using System.Linq;
using NimbusFox.FloatingDocksAPI.TileStates.Logic;
using NimbusFox.StaxelStates.Docks;
using NimbusFox.StaxelStates.Entities.Builders;
using NimbusFox.StaxelStates.Entities.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelStates.TileStates.Logic {
    public class ClaimSignTileStateEntityLogic : FloatingDockTileStateEntityLogic {
        public EntityId LogicOwner { get; private set; } = EntityId.NullEntityId;
        public TileConfiguration Configuration;
        private bool _despawn;

        public ClaimSignTileStateEntityLogic(Entity entity) : base(entity) { }

        protected override void AddSite(DockSiteConfiguration config) {
            if (config.SiteName.StartsWith(ClaimSignItemDockSite.DockCode)) {
                _dockSites.Add(new ClaimSignItemDockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config,
                    _dockSites.Count(x => x is ClaimSignItemDockSite), GetTileConfig()));
                return;
            }
            base.AddSite(config);
        }

        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            base.Update(timestep, universe);

            if (LogicOwner == EntityId.NullEntityId) {
                var blob = BlobAllocator.Blob(true);

                blob.SetString("tile", Configuration.Code);
                blob.FetchBlob("location").SetVector3I(Location);
                blob.SetBool("ignoreSpawn", _despawn);

                var entities = new Lyst<Entity>();

                universe.FindAllEntitiesInRange(entities, Location.ToVector3D(), 1F, EntityRequirements);

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    LogicOwner = tileEntity.Id;
                } else {
                    LogicOwner = SpawnEntity(blob, universe).Id;
                }

                Blob.Deallocate(ref blob);
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            base.Construct(arguments, entityUniverseFacade);
            _despawn = arguments.GetBool("despawn", false);
            Location = arguments.FetchBlob("location").GetVector3I();
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
        }

        public virtual bool EntityRequirements(Entity entity) {
            if (entity.Removed) {
                return false;
            }

            if (entity.Logic is ClaimSignTileEntityLogic logic) {
                return Location == logic.Location;
            }

            return false;
        }

        public virtual Entity SpawnEntity(Blob config, EntityUniverseFacade entityUniverseFacade) {
            return ClaimSignTileEntityBuilder.Spawn(Location, config, entityUniverseFacade);
        }

        public override string DockVerb() {
            var player = EstatesHook.FoxCore.UserManager.GetPlayerEntities()
                .First(x => !x.PlayerEntityLogic.LookingAtDockedItem().IsNull());

            return GetNearestSite(player.PlayerEntityLogic.TileCursorHitPoint()).DockVerb();
        }

        public override string UndockVerb() {
            return DockVerb();
        }

        public override void Store() {
            base.Store();

            Entity.Blob.SetLong("logicOwner", LogicOwner.Id);
        }

        public override void Restore() {
            base.Restore();

            if (Entity.Blob.Contains("logicOwner")) {
                LogicOwner = Entity.Blob.GetLong("logicOwner");
            }
        }
    }
}
