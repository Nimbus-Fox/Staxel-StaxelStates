﻿using NimbusFox.StaxelStates.TileStates.Logic;
using NimbusFox.StaxelStates.TileStates.Painter;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelStates.TileStates.EntityBuilders {
    public class ClaimSignTileStateEntityBuilder : DockTileStateEntityBuilder, IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public new string Kind => KindCode;
        public new static string KindCode => "nimbusfox.staxelstates.tileStateEntity.claimSign";

        EntityPainter IEntityPainterBuilder.Instance() {
            return new ClaimSignTileStateEntityPainter(this);
        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new ClaimSignTileStateEntityLogic(entity);
        }

        public static Entity Spawn(Vector3I location, Tile tile, Universe facade) {
            var entity = new Entity(facade.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("tile", tile.Configuration.Code);
            blob.SetLong("variant", tile.Variant());
            blob.FetchBlob("location").SetVector3I(location);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

            entity.Construct(blob, facade);

            facade.AddEntity(entity);

            Blob.Deallocate(ref blob);

            return entity;
        }
    }
}
