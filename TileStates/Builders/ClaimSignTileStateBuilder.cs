﻿using System;
using NimbusFox.StaxelStates.TileStates.EntityBuilders;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.StaxelStates.TileStates.Builders {
    public class ClaimSignTileStateBuilder : ITileStateBuilder, IDisposable {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.staxelstates.tileState.claimSign";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return ClaimSignTileStateEntityBuilder.Spawn(location, tile, universe);
        }
    }
}
