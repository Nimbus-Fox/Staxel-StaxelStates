﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.FloatingDocksAPI.TileStates.Painters;
using NimbusFox.FoxCore;
using NimbusFox.StaxelStates.Docks;
using NimbusFox.StaxelStates.Entities.Logic;
using NimbusFox.StaxelStates.TileStates.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelStates.TileStates.Painter {
    public class ClaimSignTileStateEntityPainter : FloatingDockTileStateEntityPainter {
        
        private bool _runBase;

        public ClaimSignTileStateEntityPainter(DockTileStateEntityBuilder builder) : base(builder) {
        }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) {
            base.ClientUpdate(timestep, entity, avatarController, facade);

            var stateEntityLogic = entity.DockTileStateEntityLogic;

            if (stateEntityLogic is ClaimSignTileStateEntityLogic == false) {
                return;
            }

            var stateLogic = (ClaimSignTileStateEntityLogic) stateEntityLogic;

            _runBase = false;

            foreach (var site in stateEntityLogic.GetSites()) {
                if (site is ClaimSignItemDockSite dockSite) {
                    if (facade.TryGetEntity(stateLogic.LogicOwner, out var ownerEntity)) {
                        if (ownerEntity.Logic is ClaimSignTileEntityLogic tileLogic) {
                            if (dockSite.Showitem == tileLogic.Items.XMark ||
                                dockSite.Showitem == tileLogic.Items.TickMark) {
                                _runBase = true;
                            }
                        }
                    }
                }
            }
        }

        public override void Render(DeviceContext graphics, Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {

            var stateEntityLogic = entity.DockTileStateEntityLogic;

            var bottomCenter = stateEntityLogic.GetBottomCenter();

            if (renderMode == RenderMode.Inside) {
                return;
            }

            if (stateEntityLogic.GetContainerState() == ContainerState.Closed) {
                return;
            }

            if (renderMode == RenderMode.Shadow) {
                if (!PlayerInRange(EntityPosition(renderTimestep, entity), avatarController, renderTimestep,
                    Constants.DockedItemRenderShadowCullDistanceSquared)) {
                    return;
                }
            } else if (!PlayerInRange(EntityPosition(renderTimestep, entity), avatarController, renderTimestep,
                Constants.DockedItemCountRenderCullDistanceSquared)) {
                return;
            }

            if (_runBase) {
                base.Render(graphics, matrix, renderOrigin, entity, avatarController, renderTimestep, renderMode);
                BillboardNumberRenderer.Purge();
                return;
            }

            foreach (var dock in stateEntityLogic.GetSites()) {
                if (dock is ClaimSignItemDockSite site) {

                    if (dock.IsEmpty()) {
                        continue;
                    }

                    var dockedWorldPosition = site.GetItemDockedWorldPosition(bottomCenter);

                    var dockedMatrix = ItemConfiguration.DetermineDockedMatrix(
                        Matrix.CreateFromYawPitchRoll((float)site.Config.Rotation.X, (float)site.Config.Rotation.Y, (float)site.Config.Rotation.Z).ToMatrix4F(), matrix, renderOrigin,
                        dock.DockedItem.Stack.Item, dockedWorldPosition);
                    ClientContext.ItemRendererManager.RenderInWorldFullSized(dock.DockedItem.Stack.Item, graphics, dockedMatrix, site.Config.Compact, false);
                }
            }
        }
    }
}
