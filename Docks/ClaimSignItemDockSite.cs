﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FloatingDocksAPI.DockSites;
using NimbusFox.FoxCore.Dependencies.Harmony;
using NimbusFox.StaxelStates.Classes;
using NimbusFox.StaxelStates.Components;
using NimbusFox.StaxelStates.Entities.Logic;
using NimbusFox.StaxelStates.TileStates.Logic;
using Plukit.Base;
using Staxel.Destruction;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelStates.Docks {
    public class ClaimSignItemDockSite : FloatingDockSite {
        private TileConfiguration _tileConfiguration;
        private TilesComponent _tiles;
        private ItemsComponent _items;
        public int Index { get; }
        public string Showitem { get; private set; }

        public static string DockCode => "ClaimSignItemDock";

        public ClaimSignItemDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig, int index,
            TileConfiguration parent) : base(entity, id, dockSiteConfig, parent) {
            Index = index;
            _tileConfiguration = parent;

            var emptyBlob = BlobAllocator.Blob(true);

            var itemsComponent = _tileConfiguration.Components.Select<ItemsComponent>().FirstOrDefault();

            _items = itemsComponent ?? new ItemsComponent(emptyBlob);

            var tilesComponent = _tileConfiguration.Components.Select<TilesComponent>().FirstOrDefault();

            _tiles = tilesComponent ?? new TilesComponent(emptyBlob);

            Blob.Deallocate(ref emptyBlob);
        }

        public override bool TryUndock(PlayerEntityLogic player, EntityUniverseFacade facade, int quantity) {
            if (player != null) {
                if (_entity.Logic is ClaimSignTileEntityLogic logic) {
                    if (Showitem == _items.XMark) {
                        Cancel(logic, EstatesHook.FoxCore.WorldManager.Universe);
                    }
                }
            }

            return false;
        }

        public override bool TryDock(Entity entity, EntityUniverseFacade facade, ItemStack stack, uint rotation) {
            return entity?.PlayerEntityLogic == null;
        }

        private void Confirm(ClaimSignTileEntityLogic logic) { }

        private void Cancel(ClaimSignTileEntityLogic logic, EntityUniverseFacade facade) {
            logic.Reset(facade);
        }

        public override string UndockVerb() {
            return Index == 0 ? "nimbusfox.staxelstates.verb.confirm" : "nimbusfox.staxelstates.verb.cancel";
        }

        public override string DockVerb() {
            return UndockVerb();
        }

        public enum ItemToDisplay {
            XMark,
            TickMark,
            GuestLicense,
            BuilderLicense
        }

        public void SetItem(ItemToDisplay item) {
            if (item == ItemToDisplay.XMark) {
                Showitem = _items.XMark;
            }

            if (item == ItemToDisplay.TickMark) {
                Showitem = _items.TickMark;
            }

            if (item == ItemToDisplay.GuestLicense) {
                Showitem = _items.GuestLicense;
            }

            if (item == ItemToDisplay.BuilderLicense) {
                Showitem = _items.BuilderLicense;
            }

            NeedsStorage();
            EmptyWithoutExploding(EstatesHook.FoxCore.WorldManager.Universe);
            AddToDock(null, new ItemStack(FoxCore.Helpers.MakeItem(Showitem), 1));
        }
    }
}
