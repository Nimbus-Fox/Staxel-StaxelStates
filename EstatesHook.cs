﻿using System;
using System.IO;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.StaxelStates.Classes.Config;
using NimbusFox.StaxelStates.Docks;
using NimbusFox.StaxelStates.Entities.Logic;
using Plukit.Base;
using Staxel.Destruction;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates {
    public class EstatesHook : IModHookV3 {
        internal static Fox_Core FoxCore;
        internal static Config Config;
        internal static BlobDatabase Database;

        public EstatesHook() {
            FoxCore = new Fox_Core("NimbusFox", "StaxelStates", "0.1");
        }

        public void Dispose() {
            Database?.Dispose();
            Database = null;
        }

        public void GameContextInitializeInit() {
        }
        public void GameContextInitializeBefore() {
            var blob = BlobAllocator.Blob(true);

            blob.SetString("__inherits", "staxel/tileObject/base.config");
            blob.SetObject("tiles", new Tiles());
            blob.SetObject("items", new Classes.Config.Items());

            if (!FoxCore.ConfigDirectory.FileExists("signTile.config")) {
                using (var ms = new MemoryStream()) {
                    blob.SaveJsonStream(ms);

                    ms.Seek(0L, SeekOrigin.Begin);

                    FoxCore.ConfigDirectory.WriteFileStream("signTile.config", ms, true);
                }
            } else {
                using (var ms = FoxCore.ConfigDirectory.ReadFileStream("signTile.config")) {
                    ms.Seek(0L, SeekOrigin.Begin);

                    var fileblob = BlobAllocator.Blob(true);

                    fileblob.LoadJsonStream(ms);

                    blob.MergeFrom(fileblob);

                    ms.Seek(0L, SeekOrigin.Begin);

                    ms.SetLength(0);

                    blob.SaveJsonStream(ms);

                    ms.Seek(0L, SeekOrigin.Begin);

                    FoxCore.ConfigDirectory.WriteFileStream("signTile.config", ms, true);

                    Blob.Deallocate(ref fileblob);
                }
            }

            Blob.Deallocate(ref blob);
        }
        public void GameContextInitializeAfter() {
            if (FoxCore.ConfigDirectory.FileExists("config.json")) {
                Config = FoxCore.ConfigDirectory.ReadFile<Config>("config.json", true);
            } else {
                Config = new Config();
                Config.Save();
            }
        }

        public void GameContextDeinitialize() {
            Config?.Save();
        }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (universe.Server && Database == null) {
                Database = new BlobDatabase(FoxCore.SaveDirectory.ObtainFileStream("Staxelstates.db", FileMode.OpenOrCreate), FoxCore.LogError);
            }
        }

        public void UniverseUpdateAfter() {
            if (FoxCore.WorldManager.Universe.Server) {
                Database.Save();
            }
        }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
