﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.StaxelStates.Entities.Logic;
using NimbusFox.StaxelStates.Items.Builders;
using Plukit.Base;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates.Items {
    public class MarkerItem : Item {
        private MarkerItemBuilder _markerItemBuilder;

        private EntityId _connectedEntityId = EntityId.NullEntityId;
        private Entity _connectedEntity = null;
        private bool _placementProblem = false;

        private bool _needsStore = false;

        public MarkerItem(MarkerItemBuilder builder, ItemConfiguration configuration) : base(builder.Kind()) {
            _markerItemBuilder = builder;
            Configuration = configuration;
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) {
            try {
                if (_connectedEntityId != EntityId.NullEntityId) {
                    if (!entityUniverseFacade.TryGetEntity(_connectedEntityId, out _connectedEntity)) {
                        entity.Inventory.RemoveItemWithCode(Configuration.Code,
                            entity.Inventory.CountItemsWithCode(Configuration.Code), "");
                    }
                }
            } catch {
                entity.Inventory.RemoveItem(new ItemStack(this, entity.Inventory.CountItems(this)));
                EstatesHook.FoxCore.LogError("Marker Item caused a problem. Now removing from Player's inventory");
            }
        }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (!main.DownClick) {
                return;
            }

            if (entity.PlayerEntityLogic == null) {
                return;
            }

            if (entity.Inventory.ActiveItem().Item is MarkerItem == false) {
                return;
            }

            if (!entity.PlayerEntityLogic.LookingAtTile(out _, out var adjacent)) {
                return;
            }

            if (_connectedEntity.Logic is ClaimSignTileEntityLogic logic && logic.Tiles != null &&
                logic.Location != Vector3I.Zero) {

                if (logic.Pos1 != Vector3I.Zero) {
                    var cube = new VectorCubeI(logic.Pos1, adjacent);

                    if (cube.GetTileCount() <= EstatesHook.Config.MaxTiles || cube.GetTileCount() < EstatesHook.Config.MinTiles) {
                        return;
                    }

                    if (cube.GetTileCount() * EstatesHook.Config.CostPerTile > entity.Inventory.GetMoney()) {
                        return;
                    }
                }

                var markers = entity.Inventory.ActiveItem();

                if (markers.Count == 2) {
                    logic.SetPos1(adjacent);
                    entity.Inventory.RemoveItem(new ItemStack(this, 1));
                } else if (markers.Count == 1) {
                    logic.SetPos2(adjacent);
                    entity.Inventory.RemoveItem(new ItemStack(this, 1));
                }
            }
        }

        protected override void AssignFrom(Item item) { }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            _placementProblem = false;
            if (_connectedEntity == null) {
                universe.TryGetEntity(_connectedEntityId, out _connectedEntity);
            }

            if (entity.PlayerEntityLogic == null) {
                return false;
            }

            if (entity.Inventory.ActiveItem().Item is MarkerItem == false) {
                return false;
            }

            if (!entity.PlayerEntityLogic.LookingAtTile(out _, out var adjacent)) {
                return false;
            }

            if (_connectedEntity == null) {
                return false;
            }

            if (_connectedEntity.Logic is ClaimSignTileEntityLogic logic && logic.Tiles != null && logic.Location != Vector3I.Zero) {
                var markers = entity.Inventory.ActiveItem();

                if (markers.Count == 2) {
                    if (adjacent.X <= logic.Location.X + EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.X >= logic.Location.X - EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.Z <= logic.Location.Z + EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.Z >= logic.Location.Z - EstatesHook.Config.FirstConeMaxDistanceFromSign) {
                        if (!EstatesHook.Config.IncludeY) {
                            if (adjacent.Y != logic.Location.Y) {
                                return false;
                            }
                        }
                        previews.Add(adjacent, Helpers.MakeTile(logic.Tiles.ConeBottom));
                    }
                } else if (markers.Count == 1) {
                    if (logic.Pos1 == Vector3I.Zero) {
                        return false;
                    }
                    if (!EstatesHook.Config.IncludeY) {
                        if (adjacent.Y != logic.Location.Y) {
                            return false;
                        }
                    }

                    var cube = new VectorCubeI(logic.Pos1, adjacent);

                    if (cube.GetTileCount() <= EstatesHook.Config.MaxTiles || cube.GetTileCount() < EstatesHook.Config.MinTiles) {
                        _placementProblem = true;
                    }

                    if (cube.GetTileCount() * EstatesHook.Config.CostPerTile > entity.Inventory.GetMoney()) {
                        _placementProblem = true;
                    }

                    Helpers.VectorLoop(logic.Pos1, adjacent, (x, y, z) => {
                        var renderCount = 0;
                        var zb = false;
                        var yb = false;
                        var xb = false;

                        if (EstatesHook.Config.IncludeY) {
                            yb = y == logic.Pos1.Y || y == adjacent.Y;

                            if (yb) {
                                renderCount++;
                            }
                        }

                        zb = z == logic.Pos1.Z || z == adjacent.Z;

                        if (zb) {
                            renderCount++;
                        }

                        xb = x == logic.Pos1.X || x == adjacent.X;

                        if (xb) {
                            renderCount++;
                        }

                        if (renderCount == 3 && EstatesHook.Config.IncludeY) {

                        }

                        if (renderCount == 2) {
                            previews.Add(new Vector3I(x, y, z), Helpers.MakeTile(logic.Tiles.ConeBottom, Classes.Helpers.GetConeRotation(new VectorCubeI(logic.Pos1, adjacent), new Vector3I(x, y, z))));
                        }

                        if (renderCount == 1) {
                            previews.Add(new Vector3I(x, y, z), Helpers.MakeTile(logic.Tiles.Tape, xb ? 1U : 0U));
                        }
                    });
                }
            }

            return previews.Any();
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public override bool Same(Item item) {
            if (item is MarkerItem marker) {
                if (marker._connectedEntityId == _connectedEntityId) {
                    return true;
                }
            }

            return base.Same(item);
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            LocalStore(blob);
        }

        private void LocalStore(Blob blob) {
            if (_connectedEntityId != EntityId.NullEntityId) {
                blob.SetLong("connectedEntityId", _connectedEntityId.Id);
            }
        }

        public override void StorePersistenceData(Blob blob) {
            base.StorePersistenceData(blob);
            LocalStore(blob);
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);

            if (blob.Contains("connectedEntityId")) {
                _connectedEntityId = blob.GetLong("connectedEntityId");
                EstatesHook.FoxCore.WorldManager.Universe.TryGetEntity(_connectedEntityId, out _connectedEntity);
            }
        }

        public override ItemRenderer FetchRenderer() {
            return _markerItemBuilder.Renderer;
        }

        public void SetConnected(long entityId) {
            _connectedEntityId = entityId;
        }

        public void SetConnected(EntityId entityId) {
            SetConnected(entityId.Id);
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            verb = "nimbusfox.staxelstates.verb.place";
            if (!entity.PlayerEntityLogic.LookingAtTile(out _, out var adjacent)) {
                return false;
            }

            if (_connectedEntity?.Logic is ClaimSignTileEntityLogic logic && logic.Tiles != null &&
                logic.Location != Vector3I.Zero) {
                var markers = entity.Inventory.ActiveItem();
                if (markers.Count == 2) {
                    if (adjacent.X <= logic.Location.X + EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.X >= logic.Location.X - EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.Z <= logic.Location.Z + EstatesHook.Config.FirstConeMaxDistanceFromSign &&
                        adjacent.Z >= logic.Location.Z - EstatesHook.Config.FirstConeMaxDistanceFromSign) {
                        if (!EstatesHook.Config.IncludeY) {
                            if (adjacent.Y != logic.Location.Y) {
                                return false;
                            }
                        }
                    }
                } else {
                    var cube = new VectorCubeI(logic.Pos1, adjacent);

                    if (cube.GetTileCount() >= EstatesHook.Config.MaxTiles || cube.GetTileCount() < EstatesHook.Config.MinTiles) {
                        return false;
                    }

                    if (cube.GetTileCount() * EstatesHook.Config.CostPerTile > entity.Inventory.GetMoney()) {
                        return false;
                    }
                }
            }

            return true;
        }

        public override bool TilePlacementProblem() {
            return _placementProblem;
        }
    }
}
