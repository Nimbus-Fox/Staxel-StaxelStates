﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.StaxelStates.Items.Builders {
    public class MarkerItemBuilder : IItemBuilder, IDisposable {

        public ItemRenderer Renderer { get; private set; }

        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();
        }

        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (spare is MarkerItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var markerItem = new MarkerItem(this, configuration);
            markerItem.Restore(configuration, blob);
            return markerItem;
        }

        public string Kind() {
            return "nimbusfox.staxelstates.item.marker";
        }
    }
}
