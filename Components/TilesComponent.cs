﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.StaxelStates.Classes.Config;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Components {
    public class TilesComponent : Tiles {
        public TilesComponent(Blob config) {
            if (config == null) {
                return;
            }
            ConeBottom = config.GetString("coneBottom", "nimbusfox.staxelstates.tile.coneBottom");
            ConeTop = config.GetString("coneTop", "nimbusfox.staxelstates.tile.coneTop");
            Pipe = config.GetString("pipe", "nimbusfox.staxelstates.tile.pipe");
            Tape = config.GetString("tape", "nimbusfox.staxelstates.tile.tape");
        }
    }
}
