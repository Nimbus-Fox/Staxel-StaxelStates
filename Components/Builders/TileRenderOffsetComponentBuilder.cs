﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates.Components.Builders {
    class TileRenderOffsetComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "renderOffset";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new TileRenderOffsetComponent(config);
        }
    }
}
