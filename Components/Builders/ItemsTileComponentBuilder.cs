﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates.Components.Builders {
    public class ItemsTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "items";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new ItemsComponent(config);
        }
    }
}
