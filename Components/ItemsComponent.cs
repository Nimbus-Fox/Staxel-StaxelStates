﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Components {
    public class ItemsComponent : Classes.Config.Items {

        public ItemsComponent(Blob config) {
            if (config == null) {
                return;
            }
            BuilderLicense = config.GetString("builderLicense", "nimbusfox.staxelstates.item.builderLicense");
            GuestLicense = config.GetString("guestLicense", "nimbusfox.staxelstates.item.guestLicense");
            ClaimTool = config.GetString("claimTool", "nimbusfox.staxelstates.item.coneBottom");
            TickMark = config.GetString("tickMark", "nimbusfox.staxelstates.item.tickmark");
            XMark = config.GetString("xMark", "nimbusfox.staxelstates.item.xmark");
        }
    }
}
