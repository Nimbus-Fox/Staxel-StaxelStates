﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.StaxelStates.Components {
    public class TileRenderOffsetComponent {
        public Vector3D Offset { get; } = Vector3D.Zero;

        public TileRenderOffsetComponent(Blob config) {
            if (config.Contains("y")) {
                Offset = config.GetVector3D();
            }
        }
    }
}
