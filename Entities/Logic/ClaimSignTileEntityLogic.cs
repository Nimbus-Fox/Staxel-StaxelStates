﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.StaxelStates.Classes;
using NimbusFox.StaxelStates.Classes.Config;
using NimbusFox.StaxelStates.Classes.DatabaseRecords;
using NimbusFox.StaxelStates.Components;
using NimbusFox.StaxelStates.Docks;
using NimbusFox.StaxelStates.Items;
using NimbusFox.StaxelStates.TileStates.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Destruction;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Helpers = NimbusFox.FoxCore.Helpers;

namespace NimbusFox.StaxelStates.Entities.Logic {
    public class ClaimSignTileEntityLogic : EntityLogic {

        public Entity Entity { get; }
        public Dictionary<long, DateTime> ShowFor { get; private set; } = new Dictionary<long, DateTime>();
        public string Owner { get; private set; }
        public Vector3I Location { get; private set; }
        private Blob _constructData;
        private bool _store;
        private TileConfiguration _configuration;
        public ItemsComponent Items { get; private set; }
        public TilesComponent Tiles { get; private set; }
        private Guid _guid = Guid.Empty;
        internal ClaimRecord Record;
        internal bool Render = false;

        public Vector3I Pos1 { get; private set; } = Vector3I.Zero;
        public Vector3I Pos2 { get; private set; } = Vector3I.Zero;

        public ClaimSignTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            foreach (var show in new Dictionary<long, DateTime>(ShowFor)) {
                if (show.Value >= DateTime.UtcNow) {
                    ShowFor.Remove(show.Key);
                    NeedsStore();
                }
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (Owner.IsNullOrEmpty()) {
                if (entityUniverseFacade.CanRemoveTile(Entity, Location, TileAccessFlags.SynchronousWait)) {
                    ItemEntityBuilder.SpawnDroppedItem(Entity, entityUniverseFacade,
                        new ItemStack(_configuration.MakeItem(), 1), Location.ToVector3D(), Vector3D.Zero,
                        Vector3D.Zero, SpawnDroppedFlags.None);
                    entityUniverseFacade.RemoveTile(Entity, Location, TileAccessFlags.SynchronousWait);
                    EstatesHook.FoxCore.LogError($"Removing tile at X:{Location.X} Y:{Location.Y} Z:{Location.Z} due to corruption");
                }
            }

            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, out var tile)) {
                if (tile.Configuration != _configuration) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                    if (_guid != Guid.Empty && EstatesHook.Database.RecordExists(_guid)) {
                        EstatesHook.Database.RemoveRecord(_guid);
                    }
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.FetchBlob("location").GetVector3I();
            Entity.Physics.ForcedPosition(Location.ToVector3D());
            Owner = arguments.GetString("owner", Owner ?? "");
            _configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.FetchBlob("config").GetString("tile"));

            var emptyBlob = BlobAllocator.Blob(true);

            var itemsComponent = _configuration.Components.Select<ItemsComponent>().FirstOrDefault();

            if (itemsComponent != default(ItemsComponent)) {
                Items = itemsComponent;
            } else {
                Items = new ItemsComponent(emptyBlob);
            }

            var tilesComponent = _configuration.Components.Select<TilesComponent>().FirstOrDefault();

            if (tilesComponent != default(TilesComponent)) {
                Tiles = tilesComponent;
            } else {
                Tiles = new TilesComponent(emptyBlob);
            }

            if (_guid == Guid.Empty || !EstatesHook.Database.RecordExists(_guid)) {
                while (EstatesHook.Database.RecordExists(_guid)) {
                    _guid = Guid.NewGuid();
                }

                Record = EstatesHook.Database.CreateRecord<ClaimRecord>(_guid);

                Record.Owner = Owner;
                Record.SignLocation = Location;

                if (Owner.IsNullOrEmpty()) {
                    return;
                }

                Reset(entityUniverseFacade);
            } else {
                Record = EstatesHook.Database.GetRecord<ClaimRecord>(_guid);
                Owner = Record.Owner;
            }

            if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait,
                out var stateLogic)) {
                if (stateLogic is ClaimSignTileStateEntityLogic logic) {
                    foreach (var dock in logic.GetSites()) {
                        if (dock is ClaimSignItemDockSite claimDock && claimDock.IsEmpty()) {
                            if (claimDock.Index == 1) {
                                claimDock.SetItem(ClaimSignItemDockSite.ItemToDisplay.XMark);
                            }
                        }
                    }
                }
            }

            Blob.Deallocate(ref emptyBlob);

            _constructData = BlobAllocator.Blob(true);
            _constructData.AssignFrom(arguments);
            NeedsStore();
        }
        public override void Bind() { }
        public override bool Interactable() {
            return true;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            DisplayFor(entity);
        }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return true;
        }

        public override void Store() {
            if (!_store) {
                return;
            }

            StorePersistenceData(Entity.Blob);

            Entity.Blob.SetObject("showFor", ShowFor);

            Entity.Blob.SetObject("serverConfig", EstatesHook.Config);

            Entity.Blob.SetString("tile", _configuration.Code);

            Entity.Blob.FetchBlob("location").SetVector3I(Location);

            _store = false;
        }

        public override void Restore() {
            Restore(Entity.Blob);
        }

        private void Restore(Blob data) {
            if (data.Contains("showFor")) {
                ShowFor = data.GetObject("showFor", new Dictionary<long, DateTime>());
            }

            if (data.Contains("owner")) {
                Owner = data.GetString("owner");
            }

            if (data.Contains("location")) {
                Location = data.FetchBlob("location").GetVector3I();
            }

            if (data.Contains("tile")) {
                _configuration = GameContext.TileDatabase.GetTileConfiguration(data.GetString("tile"));

                var emptyBlob = BlobAllocator.Blob(true);

                var itemsComponent = _configuration.Components.Select<ItemsComponent>().FirstOrDefault();

                if (itemsComponent != default(ItemsComponent)) {
                    Items = itemsComponent;
                } else {
                    Items = new ItemsComponent(emptyBlob);
                }

                var tilesComponent = _configuration.Components.Select<TilesComponent>().FirstOrDefault();

                if (tilesComponent != default(TilesComponent)) {
                    Tiles = tilesComponent;
                } else {
                    Tiles = new TilesComponent(emptyBlob);
                }
            }

            if (data.Contains("serverConfig")) {
                EstatesHook.Config = data.GetObject<Config>("serverConfig");
            }

            if (data.Contains("pos1")) {
                Pos1 = data.FetchBlob("pos1").GetVector3I();
            }

            if (data.Contains("pos2")) {
                Pos2 = data.FetchBlob("pos2").GetVector3I();
            }

            if (data.Contains("render")) {
                Render = data.GetBool("render");
            }
        }

        public override void StorePersistenceData(Blob data) {
            if (data != Entity.Blob) {
                data.FetchBlob("constructData").AssignFrom(_constructData);
            }

            if (_guid != Guid.Empty) {
                data.SetString("guid", _guid.ToString());
            }

            if (Owner != null) {
                data.SetString("owner", Owner);
            }

            data.FetchBlob("pos1").SetVector3I(Pos1);

            data.FetchBlob("pos2").SetVector3I(Pos2);

            data.SetBool("render", Render);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("guid")) {
                _guid = Guid.Parse(data.GetString("guid"));
            }

            if (data.Contains("constructData")) {
                Construct(data.FetchBlob("constructData"), facade);
            }

            NeedsStore();

            Restore(data);
        }

        public override bool IsCollidable() {
            return false;
        }

        public void NeedsStore() {
            _store = true;
        }

        public void DisplayFor(Entity target) {
            if (!ShowFor.ContainsKey(target.Id.Id)) {
                ShowFor.Add(target.Id.Id, DateTime.UtcNow.AddSeconds(EstatesHook.Config.ShowBorderForSeconds));
                NeedsStore();
            }
        }

        internal void SetPos1(Vector3I pos1) {
            Pos1 = pos1;
            Render = true;
            NeedsStore();
        }

        internal void SetPos2(Vector3I pos2) {
            Pos2 = pos2;
            NeedsStore();
        }

        internal void Reset(EntityUniverseFacade entityUniverseFacade) {
            Pos1 = Vector3I.Zero;
            Pos2 = Vector3I.Zero;
            Render = false;

            NeedsStore();

            var owner = EstatesHook.FoxCore.UserManager.GetPlayerEntityByUid(Owner);

            var stack = new ItemStack(Helpers.MakeItem<MarkerItem>(Items.ClaimTool), 2);

            owner.Inventory.RemoveItemWithCode(Items.ClaimTool, owner.Inventory.CountItemsWithCode(Items.ClaimTool), "");

            if (stack.Item is MarkerItem item) {
                item.SetConnected(Entity.Id);
            }

            ItemEntityBuilder.SpawnDroppedItem(owner, entityUniverseFacade, stack, owner.Physics.Position,
                Vector3D.Zero, Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);
        }
    }
}
