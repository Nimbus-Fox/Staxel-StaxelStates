﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.StaxelStates.Components;
using NimbusFox.StaxelStates.Entities.Logic;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.StaxelStates.Entities.Painter {
    public class ClaimSignTileEntityPainter : EntityPainter {
        private Vector3I _pos1 = Vector3I.Zero;
        private Vector3I _pos2 = Vector3I.Zero;

        private Vector3IMap<Tile> _previews = new Vector3IMap<Tile>();

        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) { }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) {
            if (entity.Logic is ClaimSignTileEntityLogic logic) {
                if (!logic.Render) {
                    _previews.Clear();
                    _pos1 = Vector3I.Zero;
                    _pos2 = Vector3I.Zero;
                    return;
                }
                if (_pos1 != logic.Pos1 || _pos2 != logic.Pos2) {
                    _pos1 = logic.Pos1;
                    _pos2 = logic.Pos2;
                    VectorCubeI cube = null;

                    _previews.Clear();

                    if (logic.Pos1 == Vector3I.Zero && logic.Pos2 == Vector3I.Zero) {
                        return;
                    }

                    if (logic.Pos1 != Vector3I.Zero) {
                        cube = new VectorCubeI(logic.Pos1, logic.Pos1);
                    }

                    if (logic.Pos2 != Vector3I.Zero) {
                        cube = new VectorCubeI(logic.Pos1, logic.Pos2);
                    }

                    Helpers.VectorLoop(cube.Start, cube.End, (x, y, z) => {
                        var current = new Vector3I(x, y, z);
                        var renderCount = 0;
                        var zb = false;
                        var yb = false;
                        var xb = false;

                        if (EstatesHook.Config.IncludeY) {
                            yb = y == cube.Start.Y || y == cube.End.Y;

                            if (yb) {
                                renderCount++;
                            }
                        }

                        zb = z == cube.Start.Z || z == cube.End.Z;

                        if (zb) {
                            renderCount++;
                        }

                        xb = x == cube.Start.X || x == cube.End.X;

                        if (xb) {
                            renderCount++;
                        }

                        if (renderCount == 3 && EstatesHook.Config.IncludeY) {

                        }

                        if (renderCount == 2) {
                            _previews.Add(current, Helpers.MakeTile(logic.Tiles.ConeBottom, Classes.Helpers.GetConeRotation(cube, current)));
                        }

                        if (renderCount == 1) {
                            _previews.Add(current, Helpers.MakeTile(logic.Tiles.Tape, xb ? 1U : 0U));
                        }
                    });
                }
            }
        }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
            Timestep renderTimestep) { }

        public override void Render(DeviceContext graphics, Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            foreach (var map in new Dictionary<Vector3I, Tile>(_previews.ToDictionary())) {
                var position = map.Key.ToTileCenterVector3D();
                var component = map.Value.Configuration.Components.Select<TileRenderOffsetComponent>().FirstOrDefault();
                if (component != default(TileRenderOffsetComponent)) {
                    position = new Vector3D(position.X + component.Offset.X, position.Y + component.Offset.Y, position.Z + component.Offset.Z);
                }
                var tileMatrix = ItemConfiguration.DetermineDockedMatrix(
                    Matrix.CreateFromYawPitchRoll(map.Value.Configuration.GetRotationInRadians(map.Value.Variant()), 0, 0).ToMatrix4F(), matrix, renderOrigin, map.Value.MakeItem(),
                    new Vector3D(position.X, position.Y, position.Z));

                ClientContext.ItemRendererManager.RenderInWorldFullSized(map.Value.MakeItem(), graphics, tileMatrix, false, false);
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
