﻿using System.Linq;
using NimbusFox.StaxelStates.Entities.Logic;
using NimbusFox.StaxelStates.Entities.Painter;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.StaxelStates.Entities.Builders {
    public class ClaimSignTileEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {

        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.staxelstates.entity.claimSign";

        public bool IsTileStateEntityKind() {
            return false;
        }

        public void Load() {

        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new ClaimSignTileEntityLogic(entity);
        }

        EntityPainter IEntityPainterBuilder.Instance() {
            return new ClaimSignTileEntityPainter();
        }

        public static Entity Spawn(Vector3I position, Blob config, EntityUniverseFacade universe) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);

            var owner = EstatesHook.FoxCore.UserManager.GetPlayerEntities().ToList().FirstOrDefault(x => {
                x.PlayerEntityLogic.LookingAtTile(out var target, out _);
                return new Vector3I(target.X, target.Y + 1, target.Z) == position;
            });

            if (owner != default(Entity)) {
                blob.SetString("owner", owner.PlayerEntityLogic.Uid());
            }

            blob.SetString("kind", KindCode);
            blob.FetchBlob("position").SetVector3D(position.ToTileCenterVector3D());
            blob.FetchBlob("location").SetVector3I(position);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);
            blob.FetchBlob("config").MergeFrom(config);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            Blob.Deallocate(ref blob);

            return entity;
        }
    }
}
